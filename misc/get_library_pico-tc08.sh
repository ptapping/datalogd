#!/bin/bash

# Discover latest version and download location for PicoLog software AppImage
APPIMAGE_LOCATION="https://www.picotech.com"
APPIMAGE_LOCATION="${APPIMAGE_LOCATION}$(curl -s https://www.picotech.com/downloads/_lightbox/picolog-6-for-linux | grep -o '/download/software/picolog6/sr/picolog-.*-x86_64.AppImage')"

echo "PicoLog download from ${APPIMAGE_LOCATION}"
curl -O "${APPIMAGE_LOCATION}"

# Name of downloaded file
APPIMAGE_FILE="${APPIMAGE_LOCATION##*/}"

# Make AppImage executable
chmod +x "${APPIMAGE_FILE}"

# Unpack shared library file
"./${APPIMAGE_FILE}" --appimage-extract "resources/libusbtc08.so" > /dev/null
mv squashfs-root/resources/libusbtc08.so ./libusbtc08.so.2
ln -s libusbtc08.so.2 libusbtc08.so
rm -r squashfs-root

# Copy to system library directory and refresh ldconfig cache
sudo cp libusbtc08.so.2 /usr/lib/
sudo ln -s /usr/lib/libusbtc08.so.2 /usr/lib/libusbtc08.so
sudo ldconfig

