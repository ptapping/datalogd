"""
The plugins package contains the included :class:`DataSource <datalogd.DataSource>`,
:class:`DataFilter <datalogd.DataFilter>`, and :class:`DataSink <datalogd.DataSink>`
subclasses.
Some plugins will require additional python modules to be installed, or may have
other specific requirements (for example, the :class:`LibSensorsDataSource
<datalogd.plugins.libsensors_datasource.LibSensorsDataSource>` probably won't work on Windows
operating systems).
"""
