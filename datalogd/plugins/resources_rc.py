# Resource object code (Python 3)
# Created by: object code
# Created by: The Resource Compiler for Qt version 6.5.0
# WARNING! All changes made in this file will be lost!

try:
    from PySide6 import QtCore
except:pass

qt_resource_data = b"\
\x00\x00\x03f\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><pa\
th d=\x22M417.84 44\
8a15.94 15.94 0 \
01-11.35-4.72L40\
.65 75.26a16 16 \
0 0122.7-22.56l3\
65.83 368a16 16 \
0 01-11.34 27.3z\
M364.92 80c-48.0\
9 0-80 29.55-96.\
92 51-16.88-21.4\
8-48.83-51-96.92\
-51a107.37 107.3\
7 0 00-31 4.55L1\
68 112c22.26 0 4\
5.81 9 63.94 26.\
67a123 123 0 012\
1.75 28.47 16 16\
 0 0028.6 0 123 \
123 0 0121.77-28\
.51C322.19 121 3\
42.66 112 364.92\
 112c43.15 0 78.\
62 36.33 79.07 8\
1 .54 53.69-22.7\
5 99.55-57.38 13\
9.52l22.63 22.77\
c3-3.44 5.7-6.64\
 8.14-9.6 40-48.\
75 59.15-98.8 58\
.61-153C475.37 1\
30.52 425.54 80 \
364.92 80zM268 4\
32C180.38 372.51\
 91 297.6 92 193\
a83.69 83.69 0 0\
12.24-18.39L69 1\
49.14a115.1 115.\
1 0 00-9 43.49c-\
.54 54.22 18.63 \
104.27 58.61 153\
 18.77 22.87 52.\
8 59.45 131.39 1\
12.8a31.84 31.84\
 0 0036 0c20.35-\
13.81 37.7-26.5 \
52.58-38.11l-22.\
66-22.81C300.25 \
409.6 284.09 421\
.05 268 432z\x22/><\
/svg>\
\x00\x00\x01\xbf\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><pa\
th d=\x22M400 320c0\
 88.37-55.63 144\
-144 144s-144-55\
.63-144-144c0-94\
.83 103.23-222.8\
5 134.89-259.88a\
12 12 0 0118.23 \
0C296.77 97.15 4\
00 225.17 400 32\
0z\x22 fill=\x22none\x22 \
stroke=\x22currentC\
olor\x22 stroke-mit\
erlimit=\x2210\x22 str\
oke-width=\x2232\x22/>\
<path d=\x22M344 32\
8a72 72 0 01-72 \
72\x22 fill=\x22none\x22 \
stroke=\x22currentC\
olor\x22 stroke-lin\
ecap=\x22round\x22 str\
oke-linejoin=\x22ro\
und\x22 stroke-widt\
h=\x2232\x22/></svg>\
\x00\x00\x01\xf9\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><pa\
th fill=\x22none\x22 s\
troke=\x22currentCo\
lor\x22 stroke-line\
cap=\x22round\x22 stro\
ke-linejoin=\x22rou\
nd\x22 stroke-width\
=\x2232\x22 d=\x22M256 32\
v448M313.72 80A1\
11.47 111.47 0 0\
1256 96a111.47 1\
11.47 0 01-57.72\
-16M198.28 432a1\
12.11 112.11 0 0\
1115.44 0M449.99\
 144L62.01 368M4\
37.27 218a112.09\
 112.09 0 01-57.\
71-100M74.73 294\
a112.09 112.09 0\
 0157.71 100M62.\
01 144l387.98 22\
4M74.73 218a112.\
09 112.09 0 0057\
.71-100M437.27 2\
94a112.09 112.09\
 0 00-57.71 100\x22\
/></svg>\
\x00\x00\x03P\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><pa\
th fill=\x22none\x22 s\
troke=\x22currentCo\
lor\x22 stroke-line\
cap=\x22round\x22 stro\
ke-linejoin=\x22rou\
nd\x22 stroke-width\
=\x2232\x22 d=\x22M448 34\
1.37V170.61A32 3\
2 0 00432.11 143\
l-152-88.46a47.9\
4 47.94 0 00-48.\
24 0L79.89 143A3\
2 32 0 0064 170.\
61v170.76A32 32 \
0 0079.89 369l15\
2 88.46a48 48 0 \
0048.24 0l152-88\
.46A32 32 0 0044\
8 341.37z\x22/><pat\
h fill=\x22none\x22 st\
roke=\x22currentCol\
or\x22 stroke-linec\
ap=\x22round\x22 strok\
e-linejoin=\x22roun\
d\x22 stroke-width=\
\x2232\x22 d=\x22M69 153.\
99l187 110 187-1\
10M256 463.99v-2\
00\x22/><ellipse cx\
=\x22256\x22 cy=\x22152\x22 \
rx=\x2224\x22 ry=\x2216\x22/\
><ellipse cx=\x2220\
8\x22 cy=\x22296\x22 rx=\x22\
16\x22 ry=\x2224\x22/><el\
lipse cx=\x22112\x22 c\
y=\x22328\x22 rx=\x2216\x22 \
ry=\x2224\x22/><ellips\
e cx=\x22304\x22 cy=\x222\
96\x22 rx=\x2216\x22 ry=\x22\
24\x22/><ellipse cx\
=\x22400\x22 cy=\x22240\x22 \
rx=\x2216\x22 ry=\x2224\x22/\
><ellipse cx=\x2230\
4\x22 cy=\x22384\x22 rx=\x22\
16\x22 ry=\x2224\x22/><el\
lipse cx=\x22400\x22 c\
y=\x22328\x22 rx=\x2216\x22 \
ry=\x2224\x22/></svg>\
\x00\x00\x02\xe9\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><pa\
th d=\x22M326.1 231\
.9l-47.5 75.5a31\
 31 0 01-7 7 30.\
11 30.11 0 01-35\
-49l75.5-47.5a10\
.23 10.23 0 0111\
.7 0 10.06 10.06\
 0 012.3 14z\x22/><\
path d=\x22M256 64C\
132.3 64 32 164.\
2 32 287.9a223.1\
8 223.18 0 0056.\
3 148.5c1.1 1.2 \
2.1 2.4 3.2 3.5a\
25.19 25.19 0 00\
37.1-.1 173.13 1\
73.13 0 01254.8 \
0 25.19 25.19 0 \
0037.1.1l3.2-3.5\
A223.18 223.18 0\
 00480 287.9C480\
 164.2 379.7 64 \
256 64z\x22 fill=\x22n\
one\x22 stroke=\x22cur\
rentColor\x22 strok\
e-linecap=\x22round\
\x22 stroke-linejoi\
n=\x22round\x22 stroke\
-width=\x2232\x22/><pa\
th fill=\x22none\x22 s\
troke=\x22currentCo\
lor\x22 stroke-line\
cap=\x22round\x22 stro\
ke-miterlimit=\x221\
0\x22 stroke-width=\
\x2232\x22 d=\x22M256 128\
v32M416 288h-32M\
128 288H96M165.4\
9 197.49l-22.63-\
22.63M346.51 197\
.49l22.63-22.63\x22\
/></svg>\
\x00\x00\x027\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><re\
ct x=\x2248\x22 y=\x2248\x22\
 width=\x22416\x22 hei\
ght=\x22416\x22 rx=\x2296\
\x22 fill=\x22none\x22 st\
roke=\x22currentCol\
or\x22 stroke-linej\
oin=\x22round\x22 stro\
ke-width=\x2232\x22/><\
path d=\x22M388.94 \
151.56c-24.46-22\
.28-68.72-51.4-1\
32.94-51.4s-108.\
48 29.12-132.94 \
51.4a34.66 34.66\
 0 00-3.06 48.08\
l33.32 39.21a26.\
07 26.07 0 0033.\
6 5.21c15.92-9.8\
3 40.91-21.64 69\
.1-21.64s53.18 1\
1.81 69.1 21.64a\
26.07 26.07 0 00\
33.6-5.21L392 19\
9.64a34.66 34.66\
 0 00-3.06-48.08\
z\x22 fill=\x22none\x22 s\
troke=\x22currentCo\
lor\x22 stroke-line\
join=\x22round\x22 str\
oke-width=\x2232\x22/>\
</svg>\
\x00\x00\x01\x80\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><pa\
th d=\x22M448 256c0\
-106-86-192-192-\
192S64 150 64 25\
6s86 192 192 192\
 192-86 192-192z\
\x22 fill=\x22none\x22 st\
roke=\x22currentCol\
or\x22 stroke-miter\
limit=\x2210\x22 strok\
e-width=\x2232\x22/><p\
ath d=\x22M216.32 3\
34.44l114.45-69.\
14a10.89 10.89 0\
 000-18.6l-114.4\
5-69.14a10.78 10\
.78 0 00-16.32 9\
.31v138.26a10.78\
 10.78 0 0016.32\
 9.31z\x22/></svg>\
\x00\x00\x01\xee\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><pa\
th d=\x22M448 256c0\
-106-86-192-192-\
192S64 150 64 25\
6s86 192 192 192\
 192-86 192-192z\
\x22 fill=\x22none\x22 st\
roke=\x22currentCol\
or\x22 stroke-miter\
limit=\x2210\x22 strok\
e-width=\x2232\x22/><p\
ath d=\x22M250.26 1\
66.05L256 288l5.\
73-121.95a5.74 5\
.74 0 00-5.79-6h\
0a5.74 5.74 0 00\
-5.68 6z\x22 fill=\x22\
none\x22 stroke=\x22cu\
rrentColor\x22 stro\
ke-linecap=\x22roun\
d\x22 stroke-linejo\
in=\x22round\x22 strok\
e-width=\x2232\x22/><p\
ath d=\x22M256 367.\
91a20 20 0 1120-\
20 20 20 0 01-20\
 20z\x22/></svg>\
\x00\x00\x01\xdb\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><pa\
th d=\x22M448 256c0\
-106-86-192-192-\
192S64 150 64 25\
6s86 192 192 192\
 192-86 192-192z\
\x22 fill=\x22none\x22 st\
roke=\x22currentCol\
or\x22 stroke-miter\
limit=\x2210\x22 strok\
e-width=\x2232\x22/><p\
ath d=\x22M394.77 2\
46.7l-114.45-69.\
14a10.78 10.78 0\
 00-16.32 9.31v5\
3.32l-103.68-62.\
63a10.78 10.78 0\
 00-16.32 9.31v1\
38.26a10.78 10.7\
8 0 0016.32 9.31\
L264 271.81v53.3\
2a10.78 10.78 0 \
0016.32 9.31l114\
.45-69.14a10.89 \
10.89 0 000-18.6\
z\x22/></svg>\
\x00\x00\x01\x9f\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><pa\
th d=\x22M307.72 30\
2.27a8 8 0 01-3.\
72-6.75V80a48 48\
 0 00-48-48h0a48\
 48 0 00-48 48v2\
15.52a8 8 0 01-3\
.71 6.74 97.51 9\
7.51 0 00-44.19 \
86.07A96 96 0 00\
352 384a97.49 97\
.49 0 00-44.28-8\
1.73zM256 112v27\
2\x22 fill=\x22none\x22 s\
troke=\x22currentCo\
lor\x22 stroke-line\
cap=\x22round\x22 stro\
ke-miterlimit=\x221\
0\x22 stroke-width=\
\x2232\x22/><circle cx\
=\x22256\x22 cy=\x22384\x22 \
r=\x2248\x22/></svg>\
\x00\x00\x02\x1f\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><pa\
th d=\x22M304 384v-\
24c0-29 31.54-56\
.43 52-76 28.84-\
27.57 44-64.61 4\
4-108 0-80-63.73\
-144-144-144a143\
.6 143.6 0 00-14\
4 144c0 41.84 15\
.81 81.39 44 108\
 20.35 19.21 52 \
46.7 52 76v24M22\
4 480h64M208 432\
h96M256 384V256\x22\
 fill=\x22none\x22 str\
oke=\x22currentColo\
r\x22 stroke-lineca\
p=\x22round\x22 stroke\
-linejoin=\x22round\
\x22 stroke-width=\x22\
32\x22/><path d=\x22M2\
94 240s-21.51 16\
-38 16-38-16-38-\
16\x22 fill=\x22none\x22 \
stroke=\x22currentC\
olor\x22 stroke-lin\
ecap=\x22round\x22 str\
oke-linejoin=\x22ro\
und\x22 stroke-widt\
h=\x2232\x22/></svg>\
\x00\x00\x02J\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><pa\
th fill=\x22none\x22 s\
troke=\x22currentCo\
lor\x22 stroke-line\
cap=\x22round\x22 stro\
ke-linejoin=\x22rou\
nd\x22 stroke-width\
=\x2232\x22 d=\x22M256 23\
2v-80\x22/><path fi\
ll=\x22none\x22 stroke\
=\x22currentColor\x22 \
stroke-linecap=\x22\
round\x22 stroke-li\
nejoin=\x22round\x22 s\
troke-width=\x2248\x22\
 d=\x22M256 88V72M1\
32 132l-12-12\x22/>\
<circle cx=\x22256\x22\
 cy=\x22272\x22 r=\x2232\x22\
 fill=\x22none\x22 str\
oke=\x22currentColo\
r\x22 stroke-miterl\
imit=\x2210\x22 stroke\
-width=\x2232\x22/><pa\
th d=\x22M256 96a17\
6 176 0 10176 17\
6A176 176 0 0025\
6 96z\x22 fill=\x22non\
e\x22 stroke=\x22curre\
ntColor\x22 stroke-\
miterlimit=\x2210\x22 \
stroke-width=\x2232\
\x22/></svg>\
\x00\x00\x02m\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><pa\
th d=\x22M448 256c0\
-106-86-192-192-\
192S64 150 64 25\
6s86 192 192 192\
 192-86 192-192z\
\x22 fill=\x22none\x22 st\
roke=\x22currentCol\
or\x22 stroke-miter\
limit=\x2210\x22 strok\
e-width=\x2232\x22/><p\
ath d=\x22M351.82 2\
71.87v-16A96.15 \
96.15 0 00184.09\
 192m-24.2 48.17\
v16A96.22 96.22 \
0 00327.81 320\x22 \
fill=\x22none\x22 stro\
ke=\x22currentColor\
\x22 stroke-linecap\
=\x22round\x22 stroke-\
linejoin=\x22round\x22\
 stroke-width=\x223\
2\x22/><path fill=\x22\
none\x22 stroke=\x22cu\
rrentColor\x22 stro\
ke-linecap=\x22roun\
d\x22 stroke-linejo\
in=\x22round\x22 strok\
e-width=\x2232\x22 d=\x22\
M135.87 256l23.5\
9-23.6 24.67 23.\
6M376.13 256l-23\
.59 23.6-24.67-2\
3.6\x22/></svg>\
\x00\x00\x01\xd8\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><pa\
th d=\x22M112.91 12\
8A191.85 191.85 \
0 0064 254c-1.18\
 106.35 85.65 19\
3.8 192 194 106.\
2.2 192-85.83 19\
2-192 0-104.54-8\
3.55-189.61-187.\
5-192a4.36 4.36 \
0 00-4.5 4.37V15\
2\x22 fill=\x22none\x22 s\
troke=\x22currentCo\
lor\x22 stroke-line\
cap=\x22round\x22 stro\
ke-linejoin=\x22rou\
nd\x22 stroke-width\
=\x2232\x22/><path d=\x22\
M233.38 278.63l-\
79-113a8.13 8.13\
 0 0111.32-11.32\
l113 79a32.5 32.\
5 0 01-37.25 53.\
26 33.21 33.21 0\
 01-8.07-7.94z\x22/\
></svg>\
\x00\x00\x02&\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><pa\
th d=\x22M352.92 80\
C288 80 256 144 \
256 144s-32-64-9\
6.92-64c-52.76 0\
-94.54 44.14-95.\
08 96.81-1.1 109\
.33 86.73 187.08\
 183 252.42a16 1\
6 0 0018 0c96.26\
-65.34 184.09-14\
3.09 183-252.42-\
.54-52.67-42.32-\
96.81-95.08-96.8\
1z\x22 fill=\x22none\x22 \
stroke=\x22currentC\
olor\x22 stroke-lin\
ecap=\x22round\x22 str\
oke-linejoin=\x22ro\
und\x22 stroke-widt\
h=\x2232\x22/><path fi\
ll=\x22none\x22 stroke\
=\x22currentColor\x22 \
stroke-linecap=\x22\
round\x22 stroke-li\
nejoin=\x22round\x22 s\
troke-width=\x2232\x22\
 d=\x22M48 256h112l\
48-96 48 160 48-\
96 32 64h128\x22/><\
/svg>\
\x00\x00\x01\xad\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><pa\
th d=\x22M336 176h4\
0a40 40 0 0140 4\
0v208a40 40 0 01\
-40 40H136a40 40\
 0 01-40-40V216a\
40 40 0 0140-40h\
40\x22 fill=\x22none\x22 \
stroke=\x22currentC\
olor\x22 stroke-lin\
ecap=\x22round\x22 str\
oke-linejoin=\x22ro\
und\x22 stroke-widt\
h=\x2232\x22/><path fi\
ll=\x22none\x22 stroke\
=\x22currentColor\x22 \
stroke-linecap=\x22\
round\x22 stroke-li\
nejoin=\x22round\x22 s\
troke-width=\x2232\x22\
 d=\x22M176 272l80 \
80 80-80M256 48v\
288\x22/></svg>\
\x00\x00\x02;\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><pa\
th d=\x22M85.57 446\
.25h340.86a32 32\
 0 0028.17-47.17\
L284.18 82.58c-1\
2.09-22.44-44.27\
-22.44-56.36 0L5\
7.4 399.08a32 32\
 0 0028.17 47.17\
z\x22 fill=\x22none\x22 s\
troke=\x22currentCo\
lor\x22 stroke-line\
cap=\x22round\x22 stro\
ke-linejoin=\x22rou\
nd\x22 stroke-width\
=\x2232\x22/><path d=\x22\
M250.26 195.39l5\
.74 122 5.73-121\
.95a5.74 5.74 0 \
00-5.79-6h0a5.74\
 5.74 0 00-5.68 \
5.95z\x22 fill=\x22non\
e\x22 stroke=\x22curre\
ntColor\x22 stroke-\
linecap=\x22round\x22 \
stroke-linejoin=\
\x22round\x22 stroke-w\
idth=\x2232\x22/><path\
 d=\x22M256 397.25a\
20 20 0 1120-20 \
20 20 0 01-20 20\
z\x22/></svg>\
\x00\x00\x01\x88\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><pa\
th d=\x22M32 160v29\
6a8 8 0 008 8h13\
6V160a16 16 0 00\
-16-16H48a16 16 \
0 00-16 16zM320 \
48H192a16 16 0 0\
0-16 16v400h160V\
64a16 16 0 00-16\
-16zM464 208H352\
a16 16 0 00-16 1\
6v240h136a8 8 0 \
008-8V224a16 16 \
0 00-16-16z\x22 fil\
l=\x22none\x22 stroke=\
\x22currentColor\x22 s\
troke-linecap=\x22r\
ound\x22 stroke-lin\
ejoin=\x22round\x22 st\
roke-width=\x2232\x22/\
></svg>\
\x00\x00\x13\xf8\
<\
?xml version=\x221.\
0\x22 encoding=\x22UTF\
-8\x22 standalone=\x22\
no\x22?>\x0a<!-- Creat\
ed with Inkscape\
 (http://www.ink\
scape.org/) -->\x0a\
\x0a<svg\x0a   width=\x22\
46.302082mm\x22\x0a   \
height=\x2246.30208\
2mm\x22\x0a   viewBox=\
\x220 0 46.302082 4\
6.302082\x22\x0a   ver\
sion=\x221.1\x22\x0a   id\
=\x22svg652\x22\x0a   ink\
scape:version=\x221\
.2.2 (b0a8486541\
, 2022-12-01)\x22\x0a \
  sodipodi:docna\
me=\x22go.svg\x22\x0a   x\
mlns:inkscape=\x22h\
ttp://www.inksca\
pe.org/namespace\
s/inkscape\x22\x0a   x\
mlns:sodipodi=\x22h\
ttp://sodipodi.s\
ourceforge.net/D\
TD/sodipodi-0.dt\
d\x22\x0a   xmlns=\x22htt\
p://www.w3.org/2\
000/svg\x22\x0a   xmln\
s:svg=\x22http://ww\
w.w3.org/2000/sv\
g\x22>\x0a  <sodipodi:\
namedview\x0a     i\
d=\x22namedview654\x22\
\x0a     pagecolor=\
\x22#505050\x22\x0a     b\
ordercolor=\x22#fff\
fff\x22\x0a     border\
opacity=\x221\x22\x0a    \
 inkscape:showpa\
geshadow=\x220\x22\x0a   \
  inkscape:pageo\
pacity=\x220\x22\x0a     \
inkscape:pageche\
ckerboard=\x221\x22\x0a  \
   inkscape:desk\
color=\x22#505050\x22\x0a\
     inkscape:do\
cument-units=\x22mm\
\x22\x0a     showgrid=\
\x22false\x22\x0a     ink\
scape:zoom=\x223.65\
9624\x22\x0a     inksc\
ape:cx=\x22110.1206\
\x22\x0a     inkscape:\
cy=\x2298.644014\x22\x0a \
    inkscape:win\
dow-width=\x222560\x22\
\x0a     inkscape:w\
indow-height=\x2213\
71\x22\x0a     inkscap\
e:window-x=\x220\x22\x0a \
    inkscape:win\
dow-y=\x220\x22\x0a     i\
nkscape:window-m\
aximized=\x221\x22\x0a   \
  inkscape:curre\
nt-layer=\x22layer3\
\x22 />\x0a  <defs\x0a   \
  id=\x22defs649\x22 /\
>\x0a  <g\x0a     inks\
cape:groupmode=\x22\
layer\x22\x0a     id=\x22\
layer3\x22\x0a     ink\
scape:label=\x22Lay\
er 2\x22>\x0a    <circ\
le\x0a       style=\
\x22fill:#008000;st\
roke:none;stroke\
-width:0.1426\x22\x0a \
      id=\x22path29\
2\x22\x0a       cx=\x2223\
.138597\x22\x0a       \
cy=\x2223.138597\x22\x0a \
      r=\x2223.1385\
97\x22 />\x0a    <elli\
pse\x0a       style\
=\x22fill:#008000;s\
troke:#ffffff;st\
roke-width:1.363\
05;stroke-dashar\
ray:none\x22\x0a      \
 id=\x22path454\x22\x0a  \
     cx=\x2223.1385\
97\x22\x0a       cy=\x222\
3.138597\x22\x0a      \
 rx=\x2219.883223\x22\x0a\
       ry=\x2219.82\
7211\x22 />\x0a  </g>\x0a\
  <g\x0a     inksca\
pe:label=\x22Layer \
1\x22\x0a     inkscape\
:groupmode=\x22laye\
r\x22\x0a     id=\x22laye\
r1\x22\x0a     transfo\
rm=\x22translate(-8\
0.958015,-126.65\
032)\x22\x0a     sodip\
odi:insensitive=\
\x22true\x22>\x0a    <pat\
h\x0a       fill=\x22#\
ffffff\x22\x0a       d\
=\x22m 99.060806,15\
5.79735 c -0.706\
173,0 -1.350962,\
-0.12171 -1.9346\
33,-0.36513 -0.5\
83671,-0.24315 -\
1.083204,-0.5865\
8 -1.4986,-1.030\
28 -0.415661,-0.\
44371 -0.737659,\
-0.97737 -0.9657\
29,-1.60206 -0.2\
28336,-0.62441 -\
0.342371,-1.3176\
2 -0.342371,-2.0\
7989 0,-0.77522 \
0.114035,-1.4747\
8 0.342371,-2.09\
946 0.22807,-0.6\
2442 0.54901,-1.\
15623 0.962554,-\
1.59544 0.413543\
,-0.43921 0.9088\
43,-0.77735 1.48\
5635,-1.01415 0.\
577056,-0.2368 1\
.212056,-0.35533\
 1.905529,-0.355\
33 0.58129,0 1.1\
22888,0.0743 1.6\
24808,0.22277 0.\
50165,0.1487 0.9\
4086,0.35957 1.3\
1762,0.63315 0.3\
7677,0.27358 0.6\
8131,0.60405 0.9\
1387,0.99166 0.2\
3257,0.38762 0.3\
7016,0.82047 0.4\
1355,1.29831 h -\
1.35017 c -0.025\
9,-0.31856 -0.11\
113,-0.60404 -0.\
25532,-0.85593 -\
0.1442,-0.25188 \
-0.34211,-0.4651\
3 -0.59426,-0.63\
949 -0.25162,-0.\
17436 -0.55007,-\
0.30692 -0.89455\
,-0.39714 -0.344\
756,-0.0905 -0.7\
29989,-0.13573 -\
1.156233,-0.1357\
3 -0.499533,0 -0\
.95594,0.0873 -1\
.369483,0.26167 \
-0.413544,0.1743\
6 -0.768615,0.42\
968 -1.065742,0.\
76544 -0.297127,\
0.33575 -0.52757\
9,0.74824 -0.691\
092,1.23692 -0.1\
63512,0.48895 -0\
.245533,1.04961 \
-0.245533,1.6827\
5 0,0.62019 0.08\
202,1.17555 0.24\
5533,1.66662 0.1\
63513,0.49106 0.\
393965,0.90646 0\
.691092,1.24671 \
0.297127,0.34026\
 0.653521,0.6008\
7 1.069181,0.781\
58 0.415396,0.18\
098 0.872861,0.2\
712 1.372658,0.2\
712 0.723371,0 1\
.343559,-0.11404\
 1.860289,-0.342\
37 0.51673,-0.22\
807 0.94959,-0.5\
7706 1.29831,-1.\
04643 v -1.4986 \
h -2.583924 v -1\
.17554 h 3.85630\
4 v 5.00618 h -0\
.93663 l -0.3360\
2,-1.03346 c -0.\
66701,0.80089 -1\
.71344,1.20147 -\
3.139014,1.20147\
 z\x22\x0a       id=\x22p\
ath298\x22\x0a       s\
tyle=\x22stroke-wid\
th:0.264583\x22 />\x0a\
    <path\x0a      \
 fill=\x22#ffffff\x22\x0a\
       d=\x22m 109.\
68039,145.65587 \
c 0.68024,0 1.30\
677,0.12171 1.87\
96,0.36486 0.572\
56,0.24342 1.067\
86,0.58579 1.485\
63,1.02711 0.417\
52,0.44133 0.742\
95,0.9734 0.9755\
2,1.59544 0.2325\
7,0.6223 0.34872\
,1.31445 0.34872\
,2.07671 0,0.749\
3 -0.11536,1.435\
1 -0.34554,2.057\
14 -0.23046,0.62\
256 -0.55245,1.1\
5755 -0.96573,1.\
60523 -0.41355,0\
.44794 -0.90752,\
0.79586 -1.48273\
,1.04325 -0.5746\
7,0.24765 -1.206\
76,0.37121 -1.89\
574,0.37121 -0.6\
8897,0 -1.32106,\
-0.12277 -1.8957\
4,-0.3683 -0.575\
2,-0.24553 -1.07\
024,-0.59108 -1.\
48563,-1.0369 -0\
.41566,-0.44583 \
-0.73872,-0.9794\
9 -0.96891,-1.60\
205 -0.23045,-0.\
6223 -0.34581,-1\
.31234 -0.34581,\
-2.0701 0,-0.766\
5 0.11721,-1.460\
77 0.3519,-2.083\
33 0.23468,-0.62\
23 0.56092,-1.15\
412 0.97869,-1.5\
9544 0.41752,-0.\
44133 0.91282,-0\
.78264 1.48564,-\
1.02394 0.57309,\
-0.24024 1.19988\
,-0.36089 1.8801\
3,-0.36089 z m -\
0.006,9.03023 c \
0.4908,0 0.94218\
,-0.0905 1.35334\
,-0.2712 0.41116\
,-0.18097 0.7654\
4,-0.44132 1.062\
57,-0.78158 0.29\
712,-0.34025 0.5\
2864,-0.75565 0.\
69453,-1.24671 0\
.16563,-0.49081 \
0.24844,-1.04643\
 0.24844,-1.6666\
2 0,-0.62441 -0.\
0839,-1.18215 -0\
.25188,-1.67296 \
-0.16801,-0.4908\
 -0.40058,-0.904\
34 -0.69771,-1.2\
4036 -0.29712,-0\
.33602 -0.64928,\
-0.59214 -1.0559\
5,-0.76862 -0.40\
719,-0.17647 -0.\
84958,-0.26484 -\
1.32768,-0.26484\
 -0.48683,0 -0.9\
3662,0.0894 -1.3\
5017,0.26802 -0.\
41354,0.17886 -0\
.76967,0.43815 -\
1.06891,0.7784 -\
0.29951,0.34026 \
-0.53314,0.7538 \
-0.70088,1.24037\
 -0.16802,0.4865\
7 -0.25189,1.040\
08 -0.25189,1.65\
999 0,0.61992 0.\
0828,1.17555 0.2\
4845,1.66662 0.1\
6562,0.49106 0.3\
974,0.90646 0.69\
453,1.24671 0.29\
712,0.34026 0.65\
008,0.60087 1.05\
939,0.78158 0.40\
904,0.18098 0.85\
698,0.2712 1.343\
82,0.2712 z\x22\x0a   \
    id=\x22path300\x22\
\x0a       style=\x22s\
troke-width:0.26\
4583\x22 />\x0a  </g>\x0a\
</svg>\x0a\
\x00\x00\x1dw\
<\
?xml version=\x221.\
0\x22 encoding=\x22UTF\
-8\x22 standalone=\x22\
no\x22?>\x0a<!-- Creat\
ed with Inkscape\
 (http://www.ink\
scape.org/) -->\x0a\
\x0a<svg\x0a   width=\x22\
46.302082mm\x22\x0a   \
height=\x2246.30208\
2mm\x22\x0a   viewBox=\
\x220 0 46.302082 4\
6.302082\x22\x0a   ver\
sion=\x221.1\x22\x0a   id\
=\x22svg449\x22\x0a   ink\
scape:version=\x221\
.2.2 (b0a8486541\
, 2022-12-01)\x22\x0a \
  sodipodi:docna\
me=\x22stop.svg\x22\x0a  \
 xmlns:inkscape=\
\x22http://www.inks\
cape.org/namespa\
ces/inkscape\x22\x0a  \
 xmlns:sodipodi=\
\x22http://sodipodi\
.sourceforge.net\
/DTD/sodipodi-0.\
dtd\x22\x0a   xmlns=\x22h\
ttp://www.w3.org\
/2000/svg\x22\x0a   xm\
lns:svg=\x22http://\
www.w3.org/2000/\
svg\x22>\x0a  <sodipod\
i:namedview\x0a    \
 id=\x22namedview45\
1\x22\x0a     pagecolo\
r=\x22#505050\x22\x0a    \
 bordercolor=\x22#f\
fffff\x22\x0a     bord\
eropacity=\x221\x22\x0a  \
   inkscape:show\
pageshadow=\x220\x22\x0a \
    inkscape:pag\
eopacity=\x220\x22\x0a   \
  inkscape:pagec\
heckerboard=\x221\x22\x0a\
     inkscape:de\
skcolor=\x22#505050\
\x22\x0a     inkscape:\
document-units=\x22\
mm\x22\x0a     showgri\
d=\x22false\x22\x0a     i\
nkscape:zoom=\x220.\
914906\x22\x0a     ink\
scape:cx=\x22151.92\
818\x22\x0a     inksca\
pe:cy=\x22139.35858\
\x22\x0a     inkscape:\
window-width=\x2214\
52\x22\x0a     inkscap\
e:window-height=\
\x221230\x22\x0a     inks\
cape:window-x=\x225\
18\x22\x0a     inkscap\
e:window-y=\x2279\x22\x0a\
     inkscape:wi\
ndow-maximized=\x22\
0\x22\x0a     inkscape\
:current-layer=\x22\
layer1\x22 />\x0a  <de\
fs\x0a     id=\x22defs\
446\x22 />\x0a  <g\x0a   \
  inkscape:label\
=\x22Layer 1\x22\x0a     \
inkscape:groupmo\
de=\x22layer\x22\x0a     \
id=\x22layer1\x22\x0a    \
 transform=\x22tran\
slate(-64.763272\
,-65.341656)\x22>\x0a \
   <polygon\x0a    \
   fill=\x22#ed1c24\
\x22\x0a       points=\
\x2251.255,0 0,51.2\
56 0,123.743 51.\
256,175 123.744,\
175 175,123.743 \
175,51.257 123.7\
44,0 \x22\x0a       id\
=\x22polygon282\x22\x0a  \
     transform=\x22\
matrix(0.2645833\
3,0,0,0.26458333\
,64.763272,65.34\
1656)\x22 />\x0a    <p\
olygon\x0a       fi\
ll=\x22none\x22\x0a      \
 stroke=\x22#ffffff\
\x22\x0a       stroke-\
width=\x223.0044\x22\x0a \
      points=\x2253\
.948,6.5 6.5,53.\
948 6.5,121.05 5\
3.949,168.5 121.\
051,168.5 168.5,\
121.05 168.5,53.\
949 121.051,6.5 \
\x22\x0a       id=\x22pol\
ygon284\x22\x0a       \
transform=\x22matri\
x(0.26458333,0,0\
,0.26458333,64.7\
63272,65.341656)\
\x22 />\x0a    <path\x0a \
      fill=\x22#fff\
fff\x22\x0a       d=\x22m\
 74.277953,94.48\
8685 c -0.693473\
,0 -1.29831,-0.0\
8202 -1.815042,-\
0.245534 -0.5167\
31,-0.163512 -0.\
947472,-0.391847\
 -1.29196,-0.684\
741 -0.344487,-0\
.292629 -0.60616\
,-0.642938 -0.78\
4754,-1.049867 -\
0.178858,-0.4069\
29 -0.276754,-0.\
855662 -0.293952\
,-1.346729 h 1.2\
53067 c 0.03439,\
0.37465 0.133349\
,0.697706 0.2971\
27,0.968904 0.16\
3512,0.271198 0.\
372533,0.4953 0.\
626533,0.671777 \
0.254,0.176477 0\
.547952,0.307975\
 0.881856,0.3939\
65 0.33364,0.085\
99 0.687917,0.12\
9116 1.062567,0.\
129116 0.365919,\
0 0.698764,-0.03\
863 0.998008,-0.\
116152 0.299244,\
-0.07752 0.55562\
5,-0.188383 0.76\
8615,-0.332845 0\
.213254,-0.14419\
8 0.377825,-0.31\
9617 0.494241,-0\
.526257 0.116417\
,-0.206639 0.174\
361,-0.439208 0.\
174361,-0.697706\
 0,-0.271198 -0.\
05292,-0.499533 \
-0.158221,-0.684\
742 -0.105569,-0\
.185208 -0.27887\
1,-0.347662 -0.5\
19906,-0.487891 \
-0.2413,-0.1397 \
-0.556684,-0.265\
906 -0.946415,-0\
.377825 -0.38973\
1,-0.111919 -0.8\
68892,-0.225954 \
-1.437217,-0.342\
371 -0.551127,-0\
.124883 -1.02499\
5,-0.266965 -1.4\
21077,-0.426244 \
-0.396345,-0.159\
544 -0.72337,-0.\
347662 -0.981868\
,-0.565414 -0.25\
8498,-0.217223 -\
0.448998,-0.4659\
32 -0.571765,-0.\
745861 -0.122767\
,-0.279929 -0.18\
415,-0.600869 -0\
.18415,-0.962554\
 0,-0.391848 0.0\
8387,-0.753533 0\
.251883,-1.08532\
1 0.168011,-0.33\
1523 0.405871,-0\
.617802 0.713846\
,-0.859102 0.307\
975,-0.241035 0.\
676011,-0.429683\
 1.104636,-0.565\
15 0.42836,-0.13\
5731 0.903287,-0\
.203464 1.424252\
,-0.203464 0.546\
893,0 1.047485,0\
.06668 1.501775,\
0.200289 0.45428\
9,0.133615 0.845\
079,0.32729 1.17\
2368,0.58129 0.3\
2729,0.254 0.584\
465,0.563033 0.7\
7179,0.926835 0.\
187325,0.363802 \
0.287338,0.77628\
8 0.300302,1.236\
927 h -1.253067 \
c -0.137847,-1.1\
84275 -0.949589,\
-1.776412 -2.435\
225,-1.776412 -0\
.732102,0 -1.294\
077,0.134673 -1.\
685925,0.403754 \
-0.391847,0.2693\
46 -0.587904,0.6\
36323 -0.587904,\
1.10146 0,0.2024\
07 0.03651,0.377\
825 0.109802,0.5\
26521 0.07303,0.\
148696 0.197115,\
0.278871 0.37147\
5,0.39079 0.1743\
61,0.111918 0.40\
4813,0.213254 0.\
691092,0.303477 \
0.286279,0.09049\
 0.64479,0.18097\
5 1.075531,0.271\
198 0.292629,0.0\
6033 0.594254,0.\
125941 0.904346,\
0.19685 0.310092\
,0.07117 0.61145\
2,0.156368 0.904\
346,0.255323 0.2\
92629,0.09895 0.\
569383,0.220662 \
0.829998,0.36486\
 0.26035,0.14446\
3 0.488685,0.317\
765 0.684742,0.5\
20171 0.195791,0\
.202406 0.350837\
,0.442383 0.4651\
37,0.720196 0.11\
4035,0.277812 0.\
171185,0.601927 \
0.171185,0.97234\
3 0,0.430742 -0.\
09049,0.826823 -\
0.271197,1.18850\
9 -0.180711,0.36\
1685 -0.432859,0\
.6731 -0.75565,0\
.93345 -0.322792\
,0.26035 -0.7061\
73,0.463021 -1.1\
4988,0.607218 -0\
.443441,0.144728\
 -0.93001,0.2169\
59 -1.459706,0.2\
16959 z\x22\x0a       \
id=\x22path286\x22\x0a   \
    style=\x22strok\
e-width:0.264583\
\x22 />\x0a    <path\x0a \
      fill=\x22#fff\
fff\x22\x0a       d=\x22m\
 86.699611,85.68\
4145 h -3.242733\
 v 8.636529 h -1\
.304925 v -8.636\
529 h -3.223154 \
v -1.169193 h 7.\
770812 z\x22\x0a      \
 id=\x22path288\x22\x0a  \
     style=\x22stro\
ke-width:0.26458\
3\x22 />\x0a    <path\x0a\
       fill=\x22#ff\
ffff\x22\x0a       d=\x22\
m 92.33894,84.34\
7206 c 0.680244,\
0 1.306777,0.121\
708 1.8796,0.364\
86 0.572823,0.24\
3417 1.067859,0.\
585788 1.485636,\
1.027113 0.41777\
7,0.441325 0.742\
95,0.973402 0.97\
5519,1.595437 0.\
232568,0.6223 0.\
34872,1.31445 0.\
34872,2.076715 0\
,0.7493 -0.11535\
8,1.4351 -0.3455\
45,2.057135 -0.2\
30453,0.622565 -\
0.552186,1.15755\
2 -0.96573,1.605\
227 -0.413543,0.\
44794 -0.90752,0\
.795867 -1.48246\
,1.043252 -0.574\
94,0.247386 -1.2\
07029,0.371211 -\
1.896004,0.37121\
1 -0.688975,0 -1\
.321065,-0.12276\
7 -1.896004,-0.3\
683 -0.57494,-0.\
245534 -1.07024,\
-0.59108 -1.4856\
36,-1.036902 -0.\
41566,-0.445823 \
-0.738716,-0.979\
488 -0.968904,-1\
.602052 -0.23045\
2,-0.6223 -0.345\
546,-1.312334 -0\
.345546,-2.0701 \
0,-0.766498 0.11\
7211,-1.460765 0\
.352161,-2.08333\
 0.234685,-0.622\
3 0.560916,-1.15\
4112 0.978693,-1\
.595437 0.417778\
,-0.441325 0.912\
813,-0.782638 1.\
485636,-1.023938\
 0.572823,-0.240\
241 1.199356,-0.\
360891 1.879864,\
-0.360891 z m -0\
.0066,9.030229 c\
 0.490802,0 0.94\
1916,-0.09049 1.\
353344,-0.271198\
 0.411162,-0.180\
975 0.765439,-0.\
441325 1.062566,\
-0.781579 0.2971\
27,-0.340254 0.5\
28638,-0.75565 0\
.694531,-1.24671\
7 0.16563,-0.490\
802 0.248709,-1.\
046427 0.248709,\
-1.66661 0,-0.62\
4417 -0.08387,-1\
.182159 -0.25188\
4,-1.672961 -0.1\
6801,-0.490802 -\
0.400579,-0.9043\
46 -0.697706,-1.\
240366 -0.297127\
,-0.336021 -0.64\
9287,-0.592138 -\
1.056216,-0.7686\
15 -0.40693,-0.1\
76477 -0.849313,\
-0.264848 -1.327\
415,-0.264848 -0\
.486569,0 -0.936\
625,0.08943 -1.3\
50169,0.268023 -\
0.413543,0.17885\
8 -0.769937,0.43\
815 -1.069181,0.\
778404 -0.299244\
,0.340254 -0.532\
871,0.753798 -0.\
700881,1.240367 \
-0.168011,0.4865\
69 -0.251884,1.0\
40077 -0.251884,\
1.659996 0,0.619\
918 0.08281,1.17\
5543 0.248709,1.\
66661 0.165629,0\
.490802 0.397139\
,0.906463 0.6945\
31,1.246717 0.29\
7392,0.340254 0.\
650346,0.600868 \
1.059392,0.78157\
9 0.409045,0.180\
975 0.856985,0.2\
71198 1.343554,0\
.271198 z\x22\x0a     \
  id=\x22path290\x22\x0a \
      style=\x22str\
oke-width:0.2645\
83\x22 />\x0a    <path\
\x0a       fill=\x22#f\
fffff\x22\x0a       d=\
\x22m 98.753234,84.\
514952 h 3.67559\
6 c 0.32729,0 0.\
64796,0.01826 0.\
96255,0.05503 0.\
31432,0.03678 0.\
61145,0.09684 0.\
89138,0.180975 0\
.27993,0.08387 0\
.53711,0.197115 \
0.77179,0.339196\
 0.23469,0.14208\
1 0.43603,0.3188\
23 0.60404,0.529\
696 0.16801,0.21\
1137 0.29925,0.4\
61962 0.39397,0.\
752475 0.0947,0.\
290777 0.14208,0\
.625475 0.14208,\
1.004358 0,0.999\
067 -0.31009,1.7\
526 -0.93027,2.2\
60864 -0.62019,0\
.508265 -1.53538\
,0.762265 -2.745\
32,0.762265 h -2\
.46751 v 3.92086\
 h -1.298306 z m\
 1.298306,4.7156\
68 h 2.49979 c 0\
.41354,0 0.7665,\
-0.03969 1.05939\
,-0.119591 0.292\
63,-0.07964 0.53\
181,-0.195792 0.\
71702,-0.348721 \
0.18521,-0.15266\
5 0.31962,-0.343\
165 0.40375,-0.5\
715 0.0839,-0.22\
8071 0.12595,-0.\
490802 0.12595,-\
0.788194 0,-0.36\
1685 -0.0635,-0.\
654579 -0.1905,-\
0.878417 -0.127,\
-0.223837 -0.297\
13,-0.398198 -0.\
51039,-0.523345 \
-0.21325,-0.1248\
84 -0.46302,-0.2\
08757 -0.7493,-0\
.251884 -0.28654\
,-0.04313 -0.586\
84,-0.06456 -0.9\
0117,-0.06456 h \
-2.45454 z\x22\x0a    \
   id=\x22path292\x22\x0a\
       style=\x22st\
roke-width:0.264\
583\x22 />\x0a  </g>\x0a<\
/svg>\x0a\
\x00\x00\x01p\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><pa\
th fill=\x22none\x22 s\
troke=\x22currentCo\
lor\x22 stroke-line\
cap=\x22round\x22 stro\
ke-linejoin=\x22rou\
nd\x22 stroke-width\
=\x2232\x22 d=\x22M48 320\
h64l64-256 64 38\
4 64-224 32 96h6\
4\x22/><circle cx=\x22\
432\x22 cy=\x22320\x22 r=\
\x2232\x22 fill=\x22none\x22\
 stroke=\x22current\
Color\x22 stroke-li\
necap=\x22round\x22 st\
roke-linejoin=\x22r\
ound\x22 stroke-wid\
th=\x2232\x22/></svg>\
\x00\x00\x02\xd6\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><pa\
th d=\x22M335.72 33\
0.76C381.73 299.\
5 416 251.34 416\
 192a160 160 0 0\
0-320 0v206.57c0\
 44.26 35.74 81.\
43 80 81.43h0c44\
.26 0 66.83-25.9\
4 77.29-40 14.77\
-19.81 41.71-81.\
56 82.43-109.24z\
\x22 fill=\x22none\x22 st\
roke=\x22currentCol\
or\x22 stroke-linec\
ap=\x22round\x22 strok\
e-linejoin=\x22roun\
d\x22 stroke-width=\
\x2232\x22/><path d=\x22M\
160 304V184c0-48\
.4 43.2-88 96-88\
h0c52.8 0 96 39.\
6 96 88\x22 fill=\x22n\
one\x22 stroke=\x22cur\
rentColor\x22 strok\
e-linecap=\x22round\
\x22 stroke-linejoi\
n=\x22round\x22 stroke\
-width=\x2232\x22/><pa\
th d=\x22M160 239c2\
5-18 79.82-15 79\
.82-15 26 0 41.1\
7 29.42 26 50.6 \
0 0-36.86 42.4-4\
1.86 61.4\x22 fill=\
\x22none\x22 stroke=\x22c\
urrentColor\x22 str\
oke-linecap=\x22rou\
nd\x22 stroke-linej\
oin=\x22round\x22 stro\
ke-width=\x2232\x22/><\
/svg>\
\x00\x00\x01b\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><pa\
th d=\x22M315.27 33\
L96 304h128l-31.\
51 173.23a2.36 2\
.36 0 002.33 2.7\
7h0a2.36 2.36 0 \
001.89-.95L416 2\
08H288l31.66-173\
.25a2.45 2.45 0 \
00-2.44-2.75h0a2\
.42 2.42 0 00-1.\
95 1z\x22 fill=\x22non\
e\x22 stroke=\x22curre\
ntColor\x22 stroke-\
linecap=\x22round\x22 \
stroke-linejoin=\
\x22round\x22 stroke-w\
idth=\x2232\x22/></svg\
>\
\x00\x00\x02\x0a\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><pa\
th d=\x22M288 193s1\
2.18-6-32-6a80 8\
0 0 1080 80\x22 fil\
l=\x22none\x22 stroke=\
\x22currentColor\x22 s\
troke-linecap=\x22r\
ound\x22 stroke-mit\
erlimit=\x2210\x22 str\
oke-width=\x2228\x22/>\
<path fill=\x22none\
\x22 stroke=\x22curren\
tColor\x22 stroke-l\
inecap=\x22round\x22 s\
troke-linejoin=\x22\
round\x22 stroke-wi\
dth=\x2228\x22 d=\x22M256\
 149l40 40-40 40\
\x22/><path d=\x22M256\
 64C150 64 64 15\
0 64 256s86 192 \
192 192 192-86 1\
92-192S362 64 25\
6 64z\x22 fill=\x22non\
e\x22 stroke=\x22curre\
ntColor\x22 stroke-\
miterlimit=\x2210\x22 \
stroke-width=\x2232\
\x22/></svg>\
\x00\x00\x02\xa3\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 class=\x22io\
nicon\x22 viewBox=\x22\
0 0 512 512\x22><pa\
th fill=\x22none\x22 s\
troke=\x22currentCo\
lor\x22 stroke-line\
cap=\x22round\x22 stro\
ke-linejoin=\x22rou\
nd\x22 stroke-width\
=\x2232\x22 d=\x22M304 16\
0l-64-64 64-64M2\
07 352l64 64-64 \
64\x22/><circle cx=\
\x22112\x22 cy=\x2296\x22 r=\
\x2248\x22 fill=\x22none\x22\
 stroke=\x22current\
Color\x22 stroke-li\
necap=\x22round\x22 st\
roke-linejoin=\x22r\
ound\x22 stroke-wid\
th=\x2232\x22/><circle\
 cx=\x22400\x22 cy=\x2241\
6\x22 r=\x2248\x22 fill=\x22\
none\x22 stroke=\x22cu\
rrentColor\x22 stro\
ke-linecap=\x22roun\
d\x22 stroke-linejo\
in=\x22round\x22 strok\
e-width=\x2232\x22/><p\
ath d=\x22M256 96h8\
4a60 60 0 0160 6\
0v212M255 416h-8\
4a60 60 0 01-60-\
60V144\x22 fill=\x22no\
ne\x22 stroke=\x22curr\
entColor\x22 stroke\
-linecap=\x22round\x22\
 stroke-linejoin\
=\x22round\x22 stroke-\
width=\x2232\x22/></sv\
g>\
"

qt_resource_name = b"\
\x00\x05\
\x00o\xa6S\
\x00i\
\x00c\x00o\x00n\x00s\
\x00\x0d\
\x0e\xc9\xb8\xb4\
\x00w\
\x00a\x00t\x00c\x00h\x00d\x00o\x00g\x00f\x00a\x00u\x00l\x00t\
\x00\x05\
\x00}\x8a\xc2\
\x00w\
\x00a\x00t\x00e\x00r\
\x00\x0e\
\x02\x11\x8d\xc3\
\x00r\
\x00e\x00s\x00o\x00u\x00r\x00c\x00e\x00_\x00f\x00i\x00l\x00e\x00s\
\x00\x10\
\x06\x0cD\xa4\
\x00t\
\x00e\x00m\x00p\x00e\x00r\x00a\x00t\x00u\x00r\x00e\x00-\x00c\x00o\x00l\x00d\
\x00\x06\
\x07\x88K]\
\x00r\
\x00a\x00n\x00d\x00o\x00m\
\x00\x05\
\x00zk\xb4\
\x00s\
\x00p\x00e\x00e\x00d\
\x00\x09\
\x0c\xaa'\x82\
\x00p\
\x00r\x00e\x00s\x00s\x00u\x00r\x00e\x002\
\x00\x04\
\x00\x07r\x89\
\x00p\
\x00l\x00a\x00y\
\x00\x05\
\x00h,\x94\
\x00a\
\x00l\x00e\x00r\x00t\
\x00\x0b\
\x0d\xb0\xcd\xc4\
\x00f\
\x00a\x00s\x00t\x00f\x00o\x00r\x00w\x00a\x00r\x00d\
\x00\x0b\
\x0c}4E\
\x00t\
\x00e\x00m\x00p\x00e\x00r\x00a\x00t\x00u\x00r\x00e\
\x00\x03\
\x00\x00s\xc8\
\x00l\
\x00u\x00x\
\x00\x09\
\x06}\x7f\xf8\
\x00s\
\x00t\x00o\x00p\x00w\x00a\x00t\x00c\x00h\
\x00\x04\
\x00\x06\xd3g\
\x00f\
\x00l\x00o\x00w\
\x00\x05\
\x00{\x03\xc2\
\x00t\
\x00i\x00m\x00e\x00r\
\x00\x08\
\x08\xa9\xe4\xf7\
\x00w\
\x00a\x00t\x00c\x00h\x00d\x00o\x00g\
\x00\x08\
\x08\xca\xa2e\
\x00p\
\x00r\x00e\x00s\x00s\x00u\x00r\x00e\
\x00\x07\
\x0d\x89P\xa7\
\x00w\
\x00a\x00r\x00n\x00i\x00n\x00g\
\x00\x08\
\x08\x8e\x85h\
\x00b\
\x00a\x00r\x00g\x00r\x00a\x00p\x00h\
\x00\x02\
\x00\x00\x07[\
\x00o\
\x00k\
\x00\x05\
\x00l\x8c4\
\x00f\
\x00a\x00u\x00l\x00t\
\x00\x07\
\x0a\xbc\x8c\xf4\
\x00d\
\x00e\x00f\x00a\x00u\x00l\x00t\
\x00\x03\
\x00\x00zl\
\x00s\
\x00p\x00l\
\x00\x07\
\x0dc\xa75\
\x00v\
\x00o\x00l\x00t\x00a\x00g\x00e\
\x00\x1a\
\x01\x11\xd1g\
\x00r\
\x00e\x00f\x00r\x00e\x00s\x00h\x00-\x00c\x00i\x00r\x00c\x00l\x00e\x00-\x00o\x00u\
\x00t\x00l\x00i\x00n\x00e\x00.\x00s\x00v\x00g\
\x00\x17\
\x06\xacWG\
\x00g\
\x00i\x00t\x00-\x00c\x00o\x00m\x00p\x00a\x00r\x00e\x00-\x00o\x00u\x00t\x00l\x00i\
\x00n\x00e\x00.\x00s\x00v\x00g\
"

qt_resource_struct = b"\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x01\x00\x00\x00\x01\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x18\x00\x00\x00\x02\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x01\xb0\x00\x00\x00\x00\x00\x01\x00\x00'\x02\
\x00\x00\x01\x88\x95b\xceh\
\x00\x00\x01\x18\x00\x00\x00\x00\x00\x01\x00\x00\x16\x9e\
\x00\x00\x01\x88\x95b\xceh\
\x00\x00\x01\xde\x00\x00\x00\x00\x00\x01\x00\x00Y\xed\
\x00\x00\x01\x88\x95b\xceh\
\x00\x00\x01<\x00\x00\x00\x00\x00\x01\x00\x00\x1b\x0f\
\x00\x00\x01\x88\x95b\xcek\
\x00\x00\x00\xc2\x00\x00\x00\x00\x00\x01\x00\x00\x0f\xa6\
\x00\x00\x01\x88\x95b\xceh\
\x00\x00\x00\xd0\x00\x00\x00\x00\x00\x01\x00\x00\x11*\
\x00\x00\x01\x88\x95b\xcee\
\x00\x00\x01\xba\x00\x00\x00\x00\x00\x01\x00\x00:\xfe\
\x00\x00\x01\x88\x95b\xcek\
\x00\x00\x00\x9a\x00\x00\x00\x00\x00\x01\x00\x00\x0a~\
\x00\x00\x01\x88\x95b\xcek\
\x00\x00\x01J\x00\x00\x00\x00\x00\x01\x00\x00\x1d\x80\
\x00\x00\x01\x88\x95b\xcek\
\x00\x00\x000\x00\x00\x00\x00\x00\x01\x00\x00\x03j\
\x00\x00\x01\x88\x95b\xcek\
\x00\x00\x00@\x00\x02\x00\x00\x00\x02\x00\x00\x00\x1a\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x00b\x00\x00\x00\x00\x00\x01\x00\x00\x05-\
\x00\x00\x01\x88\x95b\xcek\
\x00\x00\x01$\x00\x00\x00\x00\x00\x01\x00\x00\x18\xc1\
\x00\x00\x01\x88\x95b\xcek\
\x00\x00\x00\x88\x00\x00\x00\x00\x00\x01\x00\x00\x07*\
\x00\x00\x01\x88\x95b\xceh\
\x00\x00\x01\x9a\x00\x00\x00\x00\x00\x01\x00\x00%v\
\x00\x00\x01\x88\x95b\xceh\
\x00\x00\x01Z\x00\x00\x00\x00\x00\x01\x00\x00\x1f\x5c\
\x00\x00\x01\x88\x95b\xceh\
\x00\x00\x01p\x00\x00\x00\x00\x00\x01\x00\x00!\x86\
\x00\x00\x01\x88\x95b\xceh\
\x00\x00\x01\xca\x00\x00\x00\x00\x00\x01\x00\x00Xy\
\x00\x00\x01\x88\x95b\xceh\
\x00\x00\x00\xfc\x00\x00\x00\x00\x00\x01\x00\x00\x14\xfb\
\x00\x00\x01\x88\x95b\xcek\
\x00\x00\x00\xaa\x00\x00\x00\x00\x00\x01\x00\x00\x0dk\
\x00\x00\x01\x88\x95b\xcek\
\x00\x00\x01\xea\x00\x00\x00\x00\x00\x01\x00\x00\x5c\xc7\
\x00\x00\x01\x88\x95b\xceh\
\x00\x00\x01\x86\x00\x00\x00\x00\x00\x01\x00\x00#7\
\x00\x00\x01\x88\x95b\xcek\
\x00\x00\x00\xe0\x00\x00\x00\x00\x00\x01\x00\x00\x13\x1c\
\x00\x00\x01\x88\x95b\xceh\
\x00\x00\x00\x10\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\
\x00\x00\x01\x88\x95b\xceh\
\x00\x00\x01\xfe\x00\x00\x00\x00\x00\x01\x00\x00^-\
\x00\x00\x01\x88\x95b\xcek\
\x00\x00\x028\x00\x00\x00\x00\x00\x01\x00\x00`;\
\x00\x00\x01\x88\x95b\xceh\
"

def qInitResources():
    QtCore.qRegisterResourceData(0x03, qt_resource_struct, qt_resource_name, qt_resource_data)

def qCleanupResources():
    QtCore.qUnregisterResourceData(0x03, qt_resource_struct, qt_resource_name, qt_resource_data)

try:
    qInitResources()
except:pass
