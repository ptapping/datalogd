#!/bin/sh
# Convert Qt .qrc resource file to python source format.
pyside6-rcc "resources.qrc" -o "resources_rc.py"
# Add try/except block to catch pluginlib import error from resources_rc.py when
# scanning for plugins and PySide6 is not installed.
sed -i -e 's/^qInitResources()/try:\n    qInitResources()\nexcept:pass/' -e 's/from PySide6 import QtCore/try:\n    from PySide6 import QtCore\nexcept:pass/' resources_rc.py