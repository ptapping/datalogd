#include <OneWire.h>
#include <DallasTemperature.h>

// An ID string for this Arduino
#define BOARD_ID_STRING "0"

// Set of 8 digital input pins starting from this pin number
#define DIGITAL_PIN_0 2

// Run the 1-wire bus on pin 12
#define ONE_WIRE_BUS 12
#define TEMPERATURE_PRECISION 12

// For controlling read interval
#define READ_INTERVAL 1000
unsigned long previousMillis = 0;

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);

int numberOfDevices; // Number of temperature devices found

DeviceAddress tempDeviceAddress; // We'll use this variable to store a found device address

void setup(void)
{
  // Open serial port
  Serial.begin(115200);

  // Configure set of digital input pins
  for (uint8_t i = 0; i < 8; i++) {
    pinMode(DIGITAL_PIN_0 + i, INPUT);
  }

  // Start up the Dallas Temperature library
  sensors.begin();
  
  // Grab a count of devices on the wire
  numberOfDevices = sensors.getDeviceCount();

  // Loop through each device, set requested precision
  for(int i = 0; i < numberOfDevices; i++) {
    if(sensors.getAddress(tempDeviceAddress, i)) {
  		sensors.setResolution(tempDeviceAddress, TEMPERATURE_PRECISION);
    }
  }
}

void loop(void)
{
  // Record current time
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= READ_INTERVAL) {
    // Time to take some measurements
    previousMillis = currentMillis;
    
    // Issue a global temperature request to all devices on the bus
    sensors.requestTemperatures();

    // Print message start
    Serial.print("{\"board\":\"" + String(BOARD_ID_STRING) + "\",");
    Serial.print("\"timestamp\":\"" + String(previousMillis) + "\",");
    Serial.print("\"message\":\"measurement\",\"data\":[");
    
    // Read digital pins
    uint8_t d = 0;
    for (uint8_t i = 0; i < 8; i++) {
      d += digitalRead(DIGITAL_PIN_0 + i) << i;
    }
    printMeasurement("digital", "0", String(d, HEX));

    // Read analog pins
    int apins[] = {A0, A1, A2, A3, A4, A5};
    for (uint8_t i = 0; i < 6; i++) {
      Serial.print(",");
      printMeasurement("analog", String(i), String(analogRead(apins[i])));
    }
    
    // Loop through each device, print out temperature data
    for(int i = 0; i < numberOfDevices; i++) {
      if(sensors.getAddress(tempDeviceAddress, i)) {
        Serial.print(",");
        printMeasurement("temperature", formatAddress(tempDeviceAddress), String(sensors.getTempC(tempDeviceAddress), 2));
    	}
    }

    // Print message end
    Serial.println("]}");
  }
}

String formatAddress(DeviceAddress address) {
  String hex = "";
  for (uint8_t i = 0; i < 8; i++) {
    if (address[i] < 16) hex += "0";
    hex += String(address[i], HEX);
  }
  return hex;
}

void printMeasurement(String type, String id, String value) {
  Serial.print("{\"type\":\"");
  Serial.print(type);
  Serial.print("\",\"id\":\"");
  Serial.print(id);
  Serial.print("\",\"value\":\"");
  Serial.print(value);
  Serial.print("\"}");
}
