.PHONY: all version wheel doc upload clean

all: wheel doc

version:
	bash change_version.sh

wheel:
	./setup.py sdist bdist_wheel

doc:
	cd docs && $(MAKE) html

upload: wheel
	twine upload dist/*

clean:
	cd docs && $(MAKE) clean
	- rm datalogd.egg-info -r
	- rm build -r
	- rm dist -r
	- rm datalogd/__pycache__ -r
	- rm datalogd/plugins/__pycache__ -r
	- rm plugin_demos/__pycache__ -r
	- rm Pipfile.lock
	- rm installed_files.txt
