# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('../../'))


# -- Project information -----------------------------------------------------

project = 'datalogd'
copyright = '2020, Patrick Tapping'
author = 'Patrick Tapping'

# The full version, including alpha/beta/rc tags
release = '0.4.2'

master_doc = 'index'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['sphinx.ext.intersphinx',
              'sphinx.ext.autodoc',
              'sphinx.ext.viewcode',
]

intersphinx_mapping = {
    'python': ('https://docs.python.org/3', None),
    'pluginlib': ('https://pluginlib.readthedocs.io/en/latest/', None),
    'serial': ('https://pyserial.readthedocs.io/en/latest/', None),
}

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns =  ['Thumbs.db', '.DS_Store']

# Mock import some libraries, so readthedocs doesn't need need them for documentation generation.
autodoc_mock_imports = [
    "serial",
    "serial_asyncio",
    "sensors",
    "influxdb",
    "influxdb_client",
    "numpy",
    "matplotlib",
    "pyqtgraph",
    "PySide6",
    "pyvisa",
    "ThorlabsPM100",
    "picosdk"
]

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinxdoc'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'

# Some custom CSS to tweak appearance of some elements
html_css_files = ['custom.css']

# -- Options for PDF output --------------------------------------------------

# Handle Unicode characters in input with xelatex engine.
#latex_engine = "xelatex"

# Only generate the user guide, don't bother with api docs
#latex_documents = [("userguide/index", "datalogd_UserGuide.tex", "datalogd User Guide", "Patrick Tapping", "manual", False)]
