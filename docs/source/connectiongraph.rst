.. _connectiongraph:

Connection Graph
================

Specifying the plugins, their parameters, and the connections between them is performed
using the `DOT graph description language <https://en.wikipedia.org/wiki/DOT_(graph_description_language)>`_.

The default connection graph is valid, but not useful, as it simply connects a
:class:`~datalogd.NullDataSource` to a :class:`~datalogd.NullDataSink`:

.. code-block:: none
  :linenos:

  digraph {
    source [class=NullDataSource];
    sink [class=NullDataSink];
    source -> sink;
  }

The purpose of each line is:

1.  ``digraph`` declares that this is a directed graph. This is required, as the
    flow of data is not bi-directional.
2.  This line declares a :class:`~datalogd.NullDataSource` plugin named
    "source". ``source`` is a unique label for this node, which can be any
    string such as "src" or "a". Inside the square brackets are the attributes
    for the node. The only required attribute is ``class``, which specifies the
    python class name of the plugin to use for the node. Additional attributes
    are passed to the :meth:`__init__` method of the plugin.
3.  This line declares a :class:`~datalogd.NullDataSink` plugin named ``sink``.
4.  This line adds ``sink`` as a receiver of data from ``source``. Connections
    between nodes are indicated with ``->``.

A more complicated (but just as useless!) connection graph is:

.. code-block:: none

  digraph {
    a [class=NullDataSource];
    b [class=NullDataSource];
    c [class=NullDataFilter];
    d [class=NullDataSink];
    e [class=NullDataSink];
    a -> c -> d;
    b -> c -> e;
  }

This graph has two data sources and two sinks, connected together with a common
filter. Any data produced by either of the sources will be fed to both of the
sinks.

Node Attributes
---------------

Some plugins may accept (or require) additional parameters during initialisation.
These are provided by attributes of the graph node which describe the plugin.
The :class:`~datalogd.plugins.randomwalk_datasource.RandomWalkDataSource` is one such plugin. It generates
demonstration data using a random walk algorithm, and the parameters of the algorithm can
be specified using node attributes.

.. code-block:: none

  digraph {
    a [class=RandomWalkDataSource, interval=1.5, walkers="[[100, 2.5], [50, 1], [0, 10]]"];
    b [class=LoggingDataSink];
    a -> b;
  }

The value of ``interval`` specifies how often the algorithm should run, and the
value of ``walkers`` describes how many random walkers should be used and their
starting and increment values.

Any attribute values which contain DOT punctuation (space, comma, [], {} etc)
must be enclosed in double quotes, as seen for the ``walkers`` attribute. The
enclosed string will then be interpreted as JSON to determine its type and
value, however, any double quotes in the JSON must be replaced with single
quotes, so as to not conflict with the double quotes of the DOT language.

.. note::

  To force interpretation of an attribute as a string, enclose the value in an
  additional set of single quotes. The quotes will be removed during parsing of
  the DOT. For example, ``id="1.23e4"`` will be a float, while ``id="'1.23e4'"``
  will be a string.
