datalogd - A Data Logging Daemon
================================

datalogd is a data logging daemon service which uses a source/filter/sink plugin
architecture to allow extensive customisation and maximum flexibility.
There are no strict specifications or requirements for data types, but typical
examples would be readings from environmental sensors such as temperature,
humidity, voltage or the like.

Custom data sources, filters, or sinks can be created simply by extending an
existing :class:`~datalogd.DataSource`, :class:`~datalogd.DataFilter`, or
:class:`~datalogd.DataSink` python class and placing it in a plugin directory.

Data sources, filters, and sinks can be arbitrarily connected together with a
connection digraph described using the `DOT graph description language
<https://en.wikipedia.org/wiki/DOT_(graph_description_language)>`_.

Provided data source plugins include:
  * :class:`~datalogd.plugins.libsensors_datasource.LibSensorsDataSource` -
    (Linux) computer motherboard sensors for temperature, fan speed, voltage
    etc.
  * :class:`~datalogd.plugins.serial_datasource.SerialDataSource` - generic data
    received through a serial port device. Arduino code for acquiring and
    sending data through its USB serial connection is also included.
  * :class:`~datalogd.plugins.randomwalk_datasource.RandomWalkDataSource` -
    testing or demonstration data source using a random walk algorithm.

Provided data sink plugins include:
  * :class:`~datalogd.plugins.print_datasink.PrintDataSink` - print to standard
    out or standard error streams.
  * :class:`~datalogd.plugins.file_datasink.FileDataSink` - write to a file.
  * :class:`~datalogd.plugins.logging_datasink.LoggingDataSink` - simple output
    to console using python logging system.
  * :class:`~datalogd.plugins.csv_datafilter.CSVDataFilter` - format data as a
    table of comma separated values.
  * :class:`~datalogd.plugins.influxdb_datasink.InfluxDBDataSink` - InfluxDB
    database system specialising in time-series data.
  * :class:`~datalogd.plugins.matplotlib_datasink.MatplotlibDataSink` - create a
    plot of data using matplotlib.

Provided data filter plugins include:
  * :class:`~datalogd.plugins.keyval_datafilter.KeyValDataFilter` - selecting or
    discarding data entries based on key-value pairs.
  * :class:`~datalogd.plugins.timestamp_datafilter.TimeStampDataFilter` - adding
    timestamps to data.
  * :class:`~datalogd.plugins.aggregator_datafilter.AggregatorDataFilter` -
    aggregating multiple data readings into a fixed-size buffer.


User Documentation
------------------
.. toctree::
   :maxdepth: 5

   quickstart
   plugins
   connectiongraph
   recipes/index

API Documentation
-----------------
.. toctree::
   :maxdepth: 1

   api/datalogd

.. toctree::
   :maxdepth: 3

   api/datalogd.plugins

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
