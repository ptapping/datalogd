datalogd - A Data Logging Daemon
================================

datalogd is a data logging daemon service which uses a source/filter/sink plugin architecture to
allow extensive customisation and maximum flexibility. There are no strict specifications or
requirements for data types, but typical examples would be readings from environmental sensors such
as temperature, humidity, voltage or the like.

The user guide and API documentation can be read online at `Read the Docs
<https://datalogd.readthedocs.io/>`_. Source code is available on `GitLab
<https://gitlab.com/ptapping/datalogd>`_.

Custom data sources, filters, or sinks can be created simply by extending an existing
:class:`~datalogd.DataSource`, :class:`~datalogd.DataFilter`, or :class:`~datalogd.DataSink` python
class and placing it in a plugin directory.

Data sources, filters, and sinks can be arbitrarily connected together with a connection digraph
described using the `DOT graph description language
<https://en.wikipedia.org/wiki/DOT_(graph_description_language)>`_.

Provided data source plugins include:
  * :class:`~datalogd.plugins.libsensors_datasource.LibSensorsDataSource` - (Linux) computer
    motherboard sensors for temperature, fan speed, voltage etc.
  * :class:`~datalogd.plugins.serial_datasource.SerialDataSource` - generic data received through a
    serial port device. Arduino code for acquiring and sending data through its USB serial
    connection is also included.
  * :class:`~datalogd.plugins.randomwalk_datasource.RandomWalkDataSource` - testing or demonstration
    data source using a random walk algorithm.
  * :class:`~datalogd.plugins.thorlabspm100_datasource.ThorlabsPMDataSource` - laser or light
    power measurement using the Thorlabs PM100 or PM400 power meter.
  * :class:`~datalogd.plugins.picotc08_datasource.PicoTC08DataSource` - thermocouple or other sensor
    measurements using the Pico Technologies TC-08 USB data logger.

Provided data sink plugins include:
  * :class:`~datalogd.plugins.print_datasink.PrintDataSink` - print to standard out or standard
    error streams.
  * :class:`~datalogd.plugins.file_datasink.FileDataSink` - write to a file.
  * :class:`~datalogd.plugins.logging_datasink.LoggingDataSink` - simple output to console using
    python logging system.
  * :class:`~datalogd.plugins.influxdb2_datasink.InfluxDB2DataSink` - InfluxDB 2.x database system
    specialising in time-series data.
  * :class:`~datalogd.plugins.matplotlib_datasink.MatplotlibDataSink` - create a plot of data using
    matplotlib.
  * :class:`~datalogd.plugins.pyqtgraph_datasink.PyqtgraphDataSink` - plot incoming data in realtime
    in a pyqtgraph window.

Provided data filter plugins include:
  * :class:`~datalogd.plugins.socket_datafilter.SocketDataFilter` - bridge a connection over a
    network socket.
  * :class:`~datalogd.plugins.keyval_datafilter.KeyValDataFilter` - selecting or discarding data
    entries based on key-value pairs.
  * :class:`~datalogd.plugins.timestamp_datafilter.TimeStampDataFilter` - adding timestamps to data.
  * :class:`~datalogd.plugins.aggregator_datafilter.AggregatorDataFilter` - aggregating multiple
    data readings into a fixed-size buffer.
  * :class:`~datalogd.plugins.csv_datafilter.CSVDataFilter` - format data as a table of comma
    separated values.
  * :class:`~datalogd.plugins.polynomialfunction_datafilter.PolynomialFunctionDataFilter` - apply a
    polynomial function to a value.
  * :class:`~datalogd.plugins.flowsensorcalibration_datafilter.FlowSensorCalibrationDataFilter` -
    convert a pulse rate into liquid flow rate.
  * :class:`~datalogd.plugins.coolingpower_datafilter.CoolingPowerDataFilter` - calculate power
    dissipation into a liquid using temperatures and flow rate.

See :ref:`recipes_index` for examples of how to link various data sources, filters, and sinks to
make something useful.


User Documentation
------------------
.. toctree::
   :maxdepth: 5

   quickstart
   plugins
   connectiongraph
   recipes/index

API Documentation
-----------------
.. toctree::
   :maxdepth: 1

   api/datalogd

.. toctree::
   :maxdepth: 3

   api/datalogd.plugins

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
