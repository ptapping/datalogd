.. _quickstart:

Quick Start
===========

This guide will run through the installation and configuration of the data
logging daemon service.

Installation
------------

.. note:: ``pip3`` and ``python3`` are used here because currently ``pip`` and
  ``python`` refer to python2 versions on some common Linux distributions such
  as Ubuntu. On Windows, or distributions like Arch where python3 is the
  default, ``pip`` and ``python`` may be used instead.

.. note:: These instructions assume a system-wide install, which requires root
  or administrator privileges. On Linux, either first switch to a root login
  with ``sudo -i``, or prefix all commands with ``sudo``. Alternatively, a
  user-level install can be performed by using the ``--user`` flag on ``pip`` or
  ``systemd`` commands, for example ``pip3 install --user ...``, or ``systemctl
  --user ...``

Ensure the ``pip`` python package manager is installed. For example::

  # Debian, Ubuntu etc.
  apt install python3-pip
  # Arch Linux
  pacman -Sy python-pip

Install using ``pip``:

.. code-block:: bash

  pip3 install --upgrade datalogd

Some plugins require additional packages. These will be listed when the plugin
is attempted to be loaded. The optional dependencies can be also be installed
with ``pip``, for example:

.. code-block:: bash

  pip3 install --upgrade pyserial pyserial-asyncio PySensors influxdb matplotlib

The executable should now be available. This will show the available
:ref:`command line parameters <commandline>`:

.. code-block:: bash

  datalogd --help

On Linux operating systems, a `systemd service file
<https://www.freedesktop.org/software/systemd/man/systemd.service.html>`_ will
be installed and enabled to run on startup. Automatic configuration to start on
alternate operating systems (such as Windows) is not yet implemented, and
therefore must be done manually. Once the configuration file has been prepared,
the service can be started with ``systemctl start datalog``.

Windows
^^^^^^^

There are several ways of getting the service to run automatically at startup.
This is one example which can be configured as a standard user without admin
privileges.

* Get the the `datalogd.xml file
  <https://gitlab.com/ptapping/datalogd/-/raw/master/misc/datalogd.xml>`_ and
  save it somewhere.
* Open Task Scheduler (windows key, type ``taskschd.msc``, enter).
* Click ``Action->Import Task...``, find and select the ``datalogd.xml`` file.
* Click ``Change User or Group...`` button, type your user name, click ``Check
  Names``, then ``OK``, and ``OK``.


Configuration
-------------

The default configuration has no function, and so will not run. Configuring the
daemon is performed by either creating or editing a configuration file, or
passing parameters on the command line.


Configuration Files
^^^^^^^^^^^^^^^^^^^

To obtain the location of the default configuration files, run with the
``--show-config-dirs`` command line option.

.. code-block:: bash

  datalogd --show-config-dirs

.. code-block:: none

  INFO:main:Default configuration file locations are:
    /etc/xdg/datalogd/datalogd.conf
    /root/.config/datalogd/datalogd.conf

All config files will be read, with any options in the later files overriding
the earlier ones. Note also that a custom config file may be specified on the
:ref:`command line <commandline>`, and will be read last. Configuration
specified as :ref:`command line parameters <commandline>` will override any
configuration read from files.

A configuration file should take the form of:

.. code-block:: none

  [datalogd]

  plugin_paths = []

  connection_graph =
    digraph {
      source [class=NullDataSource];
      sink [class=NullDataSink];
      source -> sink;
    }

The options are:

  * ``plugin_paths`` - path(s) to directories containing custom
    source/filter/sink plugins. See the :ref:`plugins` section for details on
    creating custom plugins.
  * ``connection_graph`` - declaration of plugin nodes, parameters, and the
    connections between them. See the :ref:`connectiongraph` section for details
    on the connection graph syntax.


.. _commandline:

Command Line Parameters
^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: none

  datalogd --help

.. code-block:: none

  usage: datalogd [-h] [-c FILE] [-p DIR [DIR ...]] [-g GRAPH_DOT] [--show-config-dirs]

  Run the data logging daemon service.

  optional arguments:
    -h, --help            show this help message and exit
    -c FILE, --configfile FILE
                          Path to configuration file.
    -p DIR [DIR ...], --plugindirs DIR [DIR ...]
                          Directories containing additional plugins.
    -g GRAPH_DOT, --graph-dot GRAPH_DOT
                          Connection graph specified in DOT format.
    --show-config-dirs    Display the default locations of configuration files, then exit.
