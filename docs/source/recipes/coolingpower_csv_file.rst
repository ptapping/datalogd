Water Cooling Power Dissipation to CSV File
===========================================

This recipe outlines connecting `commonly available <https://www.adafruit.com/product/381>`_
`DS18B20 <https://datasheets.maximintegrated.com/en/ds/DS18B20.pdf>`__ temperature sensors and a
fluid flow sensor (such as the `YF-S401, YF-S402B
<https://www.google.com/search?q=yf-s401+yf-s402b>`__ etc) to an Arduino, which collects data and
sends it via a USB serial connection to the host computer. The data is processed to compute power
dissipation into the cooling water, converted to comma-separated values (CSV) and written to a file.

Hardware
--------

Two temperature sensors are required, one each for the inlet and outlet water temperatures. The flow
sensor should be connected in line, preferably on the cold side. The calculations need to convert
the pulse counts from the flow sensor to a fluid flow rate. Combined with the temperature difference
of the water, the power dissipated into the liquid can be determined.

.. container:: toggle

    .. container:: header

        Get the `Arduino code from GitLab
        <https://gitlab.com/ptapping/datalogd/-/tree/master/arduino>`__ or copy
        and paste below into a new Arduino sketch. Upload it to your Arduino.

    .. literalinclude:: ../../../arduino/datalog/datalog.ino
        :language: c++
        :caption: ``datalog.ino``

Ensure the appropriate sensors are selected in the Arduino code using the define statements, for
example:

    .. code-block:: c

        #define USE_DIGITAL_PINS false
        #define USE_ANALOG_PINS false
        #define USE_DS18B20_TEMPERATURE true
        #define USE_BH1750_LUX false
        #define USE_COUNTER true

Connect the thermometer's VCC to +5 V, GND to ground, and DATA to pin 12 (or another, to match that
specified in the code for the 1-Wire bus). You may need a pullup resistor between the VCC and DATA
pins if your thermometer modules don't include one. Connect the flow meter's VCC to +5 V, GND to
ground, and sense wire to pin 7 (or another interrupt-enabled pin, to match that specified in the
code for the pulse counter). Plug the Arduino into your computer USB cable.

Recipe
------

..  literalinclude:: ../../../recipes/serial_coolingpower_csv_file.config
  :language:  none
  :caption: ``recipes/serial_coolingpower_csv_file.config``

The data input and processing steps are:

  * The serial data is read in from the Arduino using the
    :class:`~datalogd.plugins.serial_datasource.SerialDataSource` data source. The data contains the
    temperatures from the thermometers and the pulse counter's number of pulses per second. The IDs
    of the temperatures correspond to the serial numbers of the thermometer devices.
  * The data is fed through a :class:`~datalogd.plugins.timestamp_datafilter.TimeStampDataFilter` to
    include timestamps on the data entries.
  * Next, the pulse counter's pulses-per-second is converted to a flow rate in litres-per-minute
    using the
    :class:`~datalogd.plugins.flowsensorcalibration_datafilter.FlowSensorCalibrationDataFilter`. The
    parameters are used in a calibration curve and have been experimentally determined for a YF-S401
    flow sensor.
  * A simple offset is applied to one of the thermometers to correct for a slight difference in
    readings between the two thermometers. This uses a
    :class:`~datalogd.plugins.polynomialfunction_datafilter.PolynomialFunctionDataFilter` to
    subtract 0.44 ℃ from the reading.

The data flow is then split to two separate paths:

  * The data is formatted into a row of comma-separated values and written to a file. This uses a
    :class:`~datalogd.plugins.csv_datafilter.CSVDataFilter` feeding in to a
    :class:`~datalogd.plugins.file_datasink.FileDataSink`.
  * The raw data is also displayed on the console using a
    :class:`~datalogd.plugins.logging_datasink.LoggingDataSink`.

