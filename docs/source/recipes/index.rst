.. _recipes_index:

Data Logging Recipes
====================

.. toctree::
   :maxdepth: 1

   randomcsv
   temperaturematplotlib
   pyqtgraph
   coolingpower_csv_file
   tc08_coolingpower_csv_file_influxdb
   rpi_temperature_influxdb_grafana
   win_serial_influxdb_grafana
   sockets
