Arduino Temperatures to InfluxDB and Grafana Display on Windows
===============================================================

.. Note::

    This recipe is outdated. There is a new 2.x version of InfluxDB which integrates a web interface
    and dashboard (Grafana is not necessary). See the documentation for the v2
    :class:`~datalogd.plugins.influxdb2_datasink.InfluxDB2DataSink` plugin for more information.


This recipe outlines connecting `commonly available <https://www.adafruit.com/product/381>`_
`DS18B20 <https://datasheets.maximintegrated.com/en/ds/DS18B20.pdf>`_
temperature sensors to an Arduino,
which collects data and sends it via a USB serial connection to a Windows
machine running `InfluxDB
<https://www.influxdata.com/products/influxdb-overview/>`_ database. `Grafana
<https://grafana.com/>`_ is used display the data over a web interface.

.. note:: No, I don't know why doing anything on Windows is so difficult.

Hardware
--------

.. container:: toggle

    .. container:: header

        Get the `Arduino code from GitLab
        <https://gitlab.com/ptapping/datalogd/-/tree/master/arduino>`_ or copy
        and paste below into a new Arduino sketch. Upload it to your Arduino.

    .. literalinclude:: ../../../arduino/datalog/datalog.ino
        :language: c++
        :caption: ``datalog.ino``

Connect VCC to +5 V, GND to ground, and DATA to pin 12 (or another, to match
that specified in the code). Plug the Arduino into your Windows machine with a
USB cable.

This is a "Pro Micro" Arduino Leonardo compatible board, connected to the
DS18B20 sensors using 3.5 mm audio jacks. The benefits of these are that readily
available plugs, sockets, splitters and extensions can be used. Don't try
plugging headphones in though, they will likely be fried!

.. image:: images/arduino-ds18b20.jpg
  :align: center

Recipe
------

..  literalinclude:: ../../../recipes/serial_temperatures_influxdb.config
  :language:  none
  :caption: ``recipes/serial_temperatures_influxdb.config``


Software
--------

Download and install InfluxDB, and configure it to run at startup:

* Download InfluxDB from https://portal.influxdata.com/downloads/ and unzip to ``Program Files``, then rename directory to ``InfluxDB``.
* Go the directory and run ``influxd.exe``.
* Run ``influx.exe``. Type ``create database datalogd`` then ``exit``.
* Hit ``Control+C`` on the ``influxd.exe`` window.
* Get the `InfluxDB.xml file <https://gitlab.com/ptapping/datalogd/-/raw/master/misc/InfluxDB.xml>`_ and save it somewhere.
* Open Task Scheduler (windows key, type ``taskschd.msc``, enter). Click ``Action->Import Task...``, select the ``InfluxDB.xml`` file. Click the ``Change User or Group...`` button, type your user name, click ``Check Names``, then ``OK``.
* Right click the ``datalogd`` entry, and select ``Run``, then click ``OK``.

Download and install Grafana:

* `Download Grafana <https://grafana.com/grafana/download?platform=windows>`_.
* Install.
* Go to ``http://localhost:3000``, login with ``admin`` ``admin``, and set a new password.
* Add an InfluxDB data source using default settings.
