System Temperatures to Matplotlib Plot
======================================

Find available sensor data from libsensors. The ``id`` field will be a composite
of the device and sensor name.

.. container:: toggle

  .. container:: header

    .. code-block:: bash

      $ sensors

  .. code-block:: none

    nvme-pci-0100
    Adapter: PCI adapter
    Composite:    +37.9°C  (low  = -273.1°C, high = +82.8°C)
                           (crit = +84.8°C)
    Sensor 1:     +37.9°C  (low  = -273.1°C, high = +65261.8°C)
    Sensor 2:     +42.9°C  (low  = -273.1°C, high = +65261.8°C)

    k10temp-pci-00c3
    Adapter: PCI adapter
    Vcore:       969.00 mV
    Vsoc:          1.09 V
    Tdie:         +40.0°C
    Tctl:         +50.0°C
    Icore:         5.00 A
    Isoc:          8.50 A

We will select both the NVME composite (solid-state disk temperature) and
k10temp Tdie (AMD CPU core temperature) using a pair of
:class:`~datalogd.plugins.keyval_datafilter.KeyValDataFilter`\ s. These make two
separate data streams which are re-joined together before being passed on to the
aggregator. The aggregator buffers one hour of data, and sends  updated data to
the :class:`~datalogd.plugins.matplotlib_datasink.MatplotlibDataSink` once every
minute.

Note that with some skilled regular expression use in ``val``, it might be
possible to use a single
:class:`~datalogd.plugins.keyval_datafilter.KeyValDataFilter` to select all
required sensor data, eliminating the need to rejoin the data streams.

..  literalinclude:: ../../../recipes/temperature_matplotlib.config
  :language:  none
  :caption: ``recipes/temperature_matplotlib.config``

.. container:: toggle

  .. container:: header

    .. code-block:: none

      $ datalogd -c recipes/temperature_matplotlib.config

  .. code-block:: none

    INFO:main:Initialising DataLogDaemon.
    INFO:DataLogDaemon:Loaded config from: /etc/xdg/datalogd/datalogd.conf, recipes/temperature_matplotlib.config
    INFO:pluginlib:Loading plugins from standard library
    INFO:DataLogDaemon:Detected source plugins: NullDataSource, LibSensorsDataSource, RandomWalkDataSource, SerialDataSource
    INFO:DataLogDaemon:Detected filter plugins: NullDataFilter, AggregatorDataFilter, CSVDataFilter, JoinDataFilter, KeyValDataFilter, TimeStampDataFilter
    INFO:DataLogDaemon:Detected sink plugins: NullDataSink, FileDataSink, InfluxDBDataSink, LoggingDataSink, MatplotlibDataSink, PrintDataSink
    INFO:DataLogDaemon:Initialising node a:LibSensorsDataSource()
    INFO:DataLogDaemon:Initialising node b:TimeStampDataFilter()
    INFO:DataLogDaemon:Initialising node c:KeyValDataFilter(key=id, val=k10temp.*Tdie)
    INFO:DataLogDaemon:Initialising node cc:KeyValDataFilter(key=id, val=nvme.*Composite)
    INFO:DataLogDaemon:Initialising node d:JoinDataFilter()
    INFO:DataLogDaemon:Initialising node e:AggregatorDataFilter(buffer_size=3600, send_every=60)
    INFO:DataLogDaemon:Initialising node s:MatplotlibDataSink(filename=temperatures_plot.pdf)
    INFO:DataLogDaemon:Connecting a:LibSensorsDataSource -> b:TimeStampDataFilter
    INFO:DataLogDaemon:Connecting b:TimeStampDataFilter -> c:KeyValDataFilter
    INFO:DataLogDaemon:Connecting c:KeyValDataFilter -> d:JoinDataFilter
    INFO:DataLogDaemon:Connecting d:JoinDataFilter -> e:AggregatorDataFilter
    INFO:DataLogDaemon:Connecting e:AggregatorDataFilter -> s:MatplotlibDataSink
    INFO:DataLogDaemon:Connecting b:TimeStampDataFilter -> cc:KeyValDataFilter
    INFO:DataLogDaemon:Connecting cc:KeyValDataFilter -> d:JoinDataFilter
    INFO:main:Starting event loop.
