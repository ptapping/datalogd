# This script is used by Qt Designer (pyside6-designer) to discover and register custom widgets.
# By setting the PYSIDE_DESIGNER_PLUGINS environment variable to point to the directory containing
# this script, Qt Designer will find and make available these widgets for use in new UI projects.

# See https://doc.qt.io/qtforpython-6.2/tutorials/basictutorial/uifiles.html

# For example:
# PYSIDE_DESIGNER_PLUGINS='.' pyside6-designer

# Note that for user-made, standalone QApplications, the code below can also be used so that 
# dynamically generated UIs (using QUILoader, loadUiType etc) can find the custom widgets. In this
# way, the environment variable does not need to be set before running the application. Note that 
# the code should be run after the QApplication is created, but before the .ui file is loaded.

import inspect

from PySide6 import QtDesigner

import datalogd.plugins.sensorpaneldatafilter as m

# Find subclasses of SensorWidgetBase and register them with Qt Designer
for sw_name, sw_class in inspect.getmembers(m, inspect.isclass):
    if issubclass(sw_class, m.SensorWidgetBase) and not sw_class == m.SensorWidgetBase:
        QtDesigner.QPyDesignerCustomWidgetCollection.registerCustomWidget(
            sw_class,
            tool_tip=sw_class.TOOL_TIP,
            xml=sw_class.XML
            )